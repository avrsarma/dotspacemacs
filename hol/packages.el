;;; packages.el --- hol layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author: Jan Mas Rovira <janmasrovira@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `hol-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `hol/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `hol/pre-init-PACKAGE' and/or
;;   `hol/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst hol-packages
  '(
    (hol-mode :location local)
    )
  "The list of Lisp packages required by the hol layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun hol/init-hol-mode ()
  (use-package hol-mode
    :defer t
    :init
    (progn
      ;; Put all the code here. Because I don't know how use-package works
      (load "hol-mode")
      (spacemacs/set-leader-keys-for-major-mode 'sml-mode
        "e" 'copy-region-or-line-as-hol-tactic
        "r" 'copy-region-or-line-as-hol-definition
        "t" 'copy-paragraph-as-hol-definition
        "," 'hol-print-goal
        "." 'hol-print-all-goals
        "R" 'hol-restart-goal
        "g" 'hol-do-goal-move-point
        "b" 'hol-backup
        "G" 'hol-subgoal-tactic
        "d" 'hol-drop-goal
        "D" 'hol-drop-all-goals
        "w" 'copy-symbol-to-hol
        "F" 'copy-file-up-to-point-to-hol
        "v" 'hol-toggle-show-types
        "<" 'then-newline
        )
      (add-hook 'sml-mode-hook
                '(lambda ()
                   (define-key sml-mode-map (kbd "<tab>") 'line-tab-to-tab-stop)
                   (local-unset-key (kbd "|"))
                   (setq electric-indent-chars '())
                   (auto-complete-mode)
                   )
                )
      )
    )
  )
;;; packages.el ends here
