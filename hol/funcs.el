(defun first-word-in-line ()
  (save-excursion
    (beginning-of-line-text)
    (word-at-point)
    )
  )

(defun first-word-in-paragraph ()
  (save-excursion
    (backward-paragraph)
    (word-at-point)
    )
  )

(defun line-tab-to-tab-stop ()
  "Indents the current line to the next tab stop"
  (interactive)
  (save-excursion
    (beginning-of-line)
    (tab-to-tab-stop)
    )
  (let ((pos (point)))
    (beginning-of-line-text)
    (let ((pos1 (point)))
      (goto-char (max pos1 pos))
      )
    )
  )

(defun copy-region-or-line-as-hol-tactic (start end arg)
  "Copies the region if active as a hol tactic, otherwise copies the line."
  (interactive "r\np")
  (if mark-active
      (copy-region-as-hol-tactic start end arg)
    (progn
      (copy-region-as-hol-tactic (line-beginning-position) (line-end-position) arg)
      (evil-next-line-first-non-blank))
    )
  )

(defun hol-do-goal-move-point ()
  (interactive)
  (hol-do-goal nil)
  (search-forward "`" nil nil)
  (evil-next-line-first-non-blank)
  )

(defun copy-region-or-line-as-hol-definition (start end arg)
  "Copies the region if active as a hol definition, otherwise copies the line."
  (interactive "r\np")
  (if mark-active
      (copy-region-as-hol-definition start end arg)
    (progn
      (if (string= (first-word-in-line) "open")
          (copy-region-as-hol-definition-quietly (line-beginning-position) (line-end-position))
        (copy-region-as-hol-definition (line-beginning-position) (line-end-position) arg))
      (evil-next-line-first-non-blank))
    )
  )

(defun copy-symbol-to-hol ()
  "Copies the symbol under the point to the hol interpreter."
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'symbol)))
    (copy-region-as-hol-definition (car bounds) (cdr bounds) 0)
    )
  )

(defun copy-paragraph-as-hol-definition (arg)
  "Copies the current paragraph as a hol definition and forwards to the next one."
  (interactive "p")
  (save-mark-and-excursion
    (mark-paragraph)
    (if (string= (first-word-in-paragraph) "open")
        (copy-region-as-hol-definition-quietly (region-beginning) (region-end))
    (copy-region-as-hol-definition (region-beginning) (region-end) arg))
    )
  (forward-paragraph)
  )

(defun copy-file-up-to-point-to-hol ()
  "Copies the contents of the file up to the point to the hol interpreter."
  (interactive)
  (save-excursion
    (forward-paragraph)
    (copy-region-as-hol-definition-quietly 1 (point))
    )
  )

(defun then-newline ()
  "Adds \">>\" to the end of the line and goes to the next line."
  (interactive)
  (save-excursion
    (end-of-line)
    (insert " >>"))
  )
